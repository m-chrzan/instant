#!/bin/bash

echo "Compilable programs:"
for file in inputs/good/*.ins; do
    ./insc_llvm $file
    ./insc_jvm $file
    (
        cd inputs/good
        foo=${file##*/}
        java ${foo%.ins}
    )
    lli ${file%.ins}.bc
done

echo "Variables not defined:"
for file in inputs/badDecl/*.ins; do
    ./insc_llvm $file
    ./insc_jvm $file
done

echo "Invalid syntax:"
for file in inputs/badParse/*.ins; do
    ./insc_llvm $file
    ./insc_jvm $file
done
