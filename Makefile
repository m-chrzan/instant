EXEC=stack exec
BNFC=$(EXEC) bnfc --
HAPPY=$(EXEC) happy --
ALEX=$(EXEC) alex --

all: instant-exe

instant-exe:
	stack setup
	stack build

parser: artifacts/instant.cf
	$(BNFC) -o src -p Parser artifacts/instant.cf
	$(HAPPY) -gca src/Parser/ParInstant.y
	$(ALEX) -g src/Parser/LexInstant.x

clean:
	stack clean
	rm -rf inputs/*/*.ll inputs/*/*.j inputs/*/*.bc inputs/*/*.class
