module Main where

import System.IO ( stderr, hPutStrLn )
import System.Environment ( getArgs )
import System.Exit ( exitFailure, exitSuccess )

import Parser.LexInstant
import Parser.ParInstant
import Parser.AbsInstant

import Parser.ErrM

import Compiler.Compiler

import Control.Monad.Except
import Control.Monad.Trans.Except

type ParseFun a = [Token] -> Err a

type ErrIO = ExceptT String IO

run :: String -> Target -> ParseFun Program -> String -> ErrIO ()
run n t p s = (do
   run' n t p s
   ) `catchError` (\e -> do
   lift $ hPutStrLn stderr e
   lift $ exitFailure
   )

run' :: String -> Target -> ParseFun Program -> String -> ErrIO ()
run' n t p s = let ts = myLexer s in case p ts of
           Bad s    -> do throwE "Parse failed"
           Ok  tree -> do code <- liftEither $ generateCode n tree t
                          lift $ putStrLn code
                          lift $ exitSuccess

usage :: IO ()
usage = do
  putStrLn $ unlines
    [ "usage: Call with one of the following argument combinations:"
    , "  --help                      Display this help message."
    , "  --llvm                      Compile stdin to LLVM verbosely."
    , "  --jvm <class name>          Compile stdin to JVM verbosely."
    ]
  exitFailure

main :: IO ()
main = do
  args <- getArgs
  case args of
    ["--help"] -> usage
    ["--llvm"] -> do
        runExceptT $ (lift getContents) >>= run "" Llvm pProgram
        return ()
    ["--jvm", name] -> do
        runExceptT $ (lift getContents) >>= run name Jvm pProgram
        return ()
    _ -> usage
