{-# LANGUAGE FlexibleContexts #-}
module Compiler.SymbolTable where

import qualified Data.Map.Lazy as Map
import Control.Monad.State.Lazy
import Control.Monad.RWS.Lazy

import Compiler.Error
import Parser.AbsInstant

type Table = Map.Map String Integer
type TableBuilderState = (
     Integer, -- last used location
     Table    -- symbol table
    )

-- Used to build a symbol table
type TableBuilder = StateT TableBuilderState Error

execTableBuilder :: Program -> Error TableBuilderState
execTableBuilder (Prog xs) = execStateT (buildSymbolTable xs) (1, Map.empty)

buildSymbolTable :: [Stmt] -> TableBuilder ()
buildSymbolTable symbols = mapM_ buildSymbolTableStmt symbols

putFst :: MonadState (a, b) m => a -> m ()
putFst a' = do
    modify $ \(a, b) -> (a', b)

modifySnd :: MonadState (a, b) m => (b -> b) -> m ()
modifySnd f = do
    modify $ \(a, b) -> (a, f b)

allocSlot :: TableBuilder Integer
allocSlot = do
    slot <- gets fst
    putFst $ slot + 1
    return slot

assertSymbolsDefined :: Exp -> TableBuilder ()
assertSymbolsDefined (ExpAdd e1 e2) = assertSymbolsDefinedOp e1 e2
assertSymbolsDefined (ExpSub e1 e2) = assertSymbolsDefinedOp e1 e2
assertSymbolsDefined (ExpMul e1 e2) = assertSymbolsDefinedOp e1 e2
assertSymbolsDefined (ExpDiv e1 e2) = assertSymbolsDefinedOp e1 e2
assertSymbolsDefined (ExpLit n) = return ()
assertSymbolsDefined (ExpVar (Ident x)) = do
    isDefined  <- gets (Map.member x . snd)
    if isDefined then return () else lift $ Left $ x ++ " is not defined"

assertSymbolsDefinedOp e1 e2 = do
    assertSymbolsDefined e1
    assertSymbolsDefined e2

buildSymbolTableStmt :: Stmt -> TableBuilder ()
buildSymbolTableStmt (SAss (Ident x) e) = do
    assertSymbolsDefined e
    alreadyDefined <- gets (Map.member x . snd)
    if alreadyDefined then return () else do
        slot <- allocSlot
        modifySnd $ Map.insert x slot
buildSymbolTableStmt (SExp e) = assertSymbolsDefined e
