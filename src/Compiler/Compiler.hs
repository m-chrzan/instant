module Compiler.Compiler where

import Parser.AbsInstant

import qualified Compiler.Backend.Jvm as Jvm
import qualified Compiler.Backend.Llvm as Llvm
import Compiler.SymbolTable
import Compiler.Error

data Target = Llvm | Jvm

generateCode :: String -> Program -> Target -> Error String
generateCode name p t = do
    (lastUsed, symbolTable) <- execTableBuilder p
    case t of
        Llvm -> Llvm.generateCode lastUsed symbolTable p
        Jvm -> Jvm.generateCode name lastUsed symbolTable p
