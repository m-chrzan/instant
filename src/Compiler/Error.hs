module Compiler.Error where

type Error = Either String
