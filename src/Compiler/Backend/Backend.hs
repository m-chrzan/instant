{-# LANGUAGE FlexibleContexts #-}
module Compiler.Backend.Backend where
import Control.Monad.RWS.Lazy
import qualified Data.Map.Lazy as Map

import Compiler.Error
import Compiler.SymbolTable

tellLn :: MonadWriter String m => String -> m ()
tellLn s = tell (s ++ "\n")

type Synthesizer s = RWST Table String s Error

lookupVariable :: String -> Synthesizer s Integer
lookupVariable x = do
    location <- asks (Map.lookup x)
    case location of
        Just n -> return n
        Nothing -> lift $ Left $ "undefined variable " ++ x
