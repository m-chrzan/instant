module Compiler.Backend.Llvm where

import Control.Monad.RWS.Lazy

import Parser.AbsInstant

import Compiler.Backend.Backend
import Compiler.Error
import Compiler.SymbolTable

-- The synthesizer state keeps track of the last used register
type LlvmSynthesizerState = Integer
type LlvmSynthesizer = Synthesizer LlvmSynthesizerState

allocWorkRegister :: LlvmSynthesizer String
allocWorkRegister = do
    lastRegister <- get
    put $ lastRegister + 1
    return $ workRegisterName lastRegister

workRegisterName :: Integer -> String
workRegisterName n = "%reg" ++ (show n)

getVariableRegister :: String -> LlvmSynthesizer String
getVariableRegister x = do
    registerNumber <- lookupVariable x
    return $ variableRegisterName registerNumber

variableRegisterName :: Integer -> String
variableRegisterName n = "%var" ++ (show n)

generateCode :: Integer -> Table -> Program -> Error String
generateCode lastUsed symbolTable p = do
    (_, output) <- evalRWST (synthesizeLlvmProg p) symbolTable 0
    return $ prolog ++
             (allocateVars lastUsed) ++
             output ++
             epilog

allocateVars 0 = ""
allocateVars n = (allocateVars $ n - 1) ++ (variableRegisterName n) ++ " = alloca i32\n" 

prolog = "declare void @printInt(i32)\n" ++
         "define i32 @main() {\n"

epilog = "ret i32 0\n" ++
         "}"

synthesizeLlvmProg :: Program -> LlvmSynthesizer ()
synthesizeLlvmProg (Prog p) = mapM_ synthesizeLlvmStmt p

synthesizeLlvmStmt :: Stmt -> LlvmSynthesizer ()
synthesizeLlvmStmt (SExp e) = do
    result <- synthesizeLlvmComputation e
    tellLn $ "call void @printInt(i32 " ++ (resultString result) ++ ")"
synthesizeLlvmStmt (SAss (Ident x) e) = do
    result <- synthesizeLlvmComputation e
    variableRegister <- getVariableRegister x
    tellLn $ "store i32 " ++ (resultString result) ++ ", i32* " ++ variableRegister

data Result = Literal Integer | Register String

synthesizeLlvmComputation :: Exp -> LlvmSynthesizer Result
synthesizeLlvmComputation (ExpLit n) = return $ Literal n
synthesizeLlvmComputation (ExpVar (Ident x)) = do
    variableRegister <- getVariableRegister x
    workRegister <- allocWorkRegister
    tellLn $ workRegister ++ " = load i32, i32* " ++ variableRegister
    return $ Register workRegister
synthesizeLlvmComputation (ExpAdd e1 e2) = synthesizeLlvmOpComputation "add" e1 e2
synthesizeLlvmComputation (ExpSub e1 e2) = synthesizeLlvmOpComputation "sub" e1 e2
synthesizeLlvmComputation (ExpMul e1 e2) = synthesizeLlvmOpComputation "mul" e1 e2
synthesizeLlvmComputation (ExpDiv e1 e2) = synthesizeLlvmOpComputation "sdiv" e1 e2

synthesizeLlvmOpComputation op e1 e2 = do
    result1 <- synthesizeLlvmComputation e1
    result2 <- synthesizeLlvmComputation e2
    workRegister <- allocWorkRegister
    tellLn $ workRegister ++ " = " ++ op ++ " i32 " ++ (resultString result1) ++ ", " ++ (resultString result2)
    return $ Register workRegister

resultString :: Result -> String
resultString (Register r) = r
resultString (Literal n) = show n
