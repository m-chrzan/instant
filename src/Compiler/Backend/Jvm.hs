{-# LANGUAGE FlexibleContexts #-}
module Compiler.Backend.Jvm where


import Control.Monad.RWS.Lazy

import Parser.AbsInstant

import Compiler.Backend.Backend
import Compiler.Error
import Compiler.SymbolTable

-- The synthesizer state keeps track of the maximum stack depth needed
type JvmSynthesizerState = Integer
type JvmSynthesizer = Synthesizer JvmSynthesizerState

updateMaxDepth :: Integer -> JvmSynthesizer ()
updateMaxDepth depth = modify (max depth)

generateCode :: String -> Integer -> Table -> Program -> Error String
generateCode name lastUsed symbolTable p = do
    (_, depth, output) <- runRWST (synthesizeJvmProg p) symbolTable 0
    return $ (prolog name) ++
             ".limit stack " ++ (show (depth + 1)) ++ "\n" ++
             ".limit locals " ++ (show lastUsed) ++ "\n" ++
             output ++
             epilog

prolog name = ".class public " ++ name ++ "\n" ++
    ".super java/lang/Object\n" ++
    "\n" ++
    "; standard initializer\n" ++
    ".method public <init>()V\n" ++
    "   aload_0\n" ++
    "   invokespecial java/lang/Object/<init>()V\n" ++
    "   return\n" ++
    ".end method\n" ++
    "\n" ++
    ".method public static main([Ljava/lang/String;)V\n"

epilog = "  return\n" ++
    ".end method\n"

synthesizeJvmProg :: Program -> JvmSynthesizer ()
synthesizeJvmProg (Prog p) = mapM_ synthesizeJvmStmt p

synthesizeJvmStmt :: Stmt -> JvmSynthesizer ()
synthesizeJvmStmt (SExp e) = do
    tellLn "getstatic  java/lang/System/out Ljava/io/PrintStream;"
    let e' = analyze e
    synthesizeJvmComputation e'
    tellLn "invokestatic java/lang/Integer/toString(I)Ljava/lang/String;"
    tellLn "invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V"
    updateMaxDepth $ (stackDepth e') + 1
synthesizeJvmStmt (SAss (Ident x) e) = do
    let e' = analyze e
    synthesizeJvmComputation e'
    location <- lookupVariable x
    tellLn $ istore location
    updateMaxDepth $ (stackDepth e')

istore :: Integer -> String
istore n
    | n <= 3 = "istore_" ++ (show n)
    | otherwise = "istore " ++ (show n)

-- An analyzed expression is annotated with the JVM stack depth needed to
-- evaluate it
data AnalyzedExp = AnalyzedOp OpType AnalyzedExp AnalyzedExp Integer
    | AnalyzedLit Integer Integer
    | AnalyzedVar Ident Integer

data OpType = Add | Sub | Mul | Div

stackDepth :: AnalyzedExp -> Integer
stackDepth (AnalyzedOp _ _ _ depth) = depth
stackDepth (AnalyzedLit _ depth) = depth
stackDepth (AnalyzedVar _ depth) = depth

analyze :: Exp -> AnalyzedExp
analyze (ExpAdd e1 e2) = analyzeOp Add e1 e2
analyze (ExpSub e1 e2) = analyzeOp Sub e1 e2
analyze (ExpMul e1 e2) = analyzeOp Mul e1 e2
analyze (ExpDiv e1 e2) = analyzeOp Div e1 e2
analyze (ExpLit n) = AnalyzedLit n 0
analyze (ExpVar x) = AnalyzedVar x 0

analyzeOp :: OpType -> Exp -> Exp -> AnalyzedExp
analyzeOp t e1 e2 = AnalyzedOp t e1' e2' depth where
    e1' = analyze e1
    e2' = analyze e2
    depth1 = stackDepth e1'
    depth2 = stackDepth e2'
    depth = if depth1 == depth2 then depth1 + 1 else max depth1 depth2

constInstr n
    | n <= 5 = "iconst_" ++ (show n)
    | n <= 127 = "bipush " ++ (show n)
    | otherwise = "ldc " ++ (show n)

synthesizeJvmComputation :: AnalyzedExp -> JvmSynthesizer ()
synthesizeJvmComputation (AnalyzedLit n _) = tellLn $ constInstr n
synthesizeJvmComputation (AnalyzedVar (Ident x) _) = do
    location <- lookupVariable x
    tellLn $ iload location
synthesizeJvmComputation (AnalyzedOp t e1 e2 _) = do
    let depth1 = stackDepth e1
    let depth2 = stackDepth e2
    if depth1 >= depth2 then do
        synthesizeJvmComputation e1
        synthesizeJvmComputation e2
    else do
        synthesizeJvmComputation e2
        synthesizeJvmComputation e1
        if not (commutative t) then tellLn "swap" else return ()
    tellLn $ jvmOp t

iload :: Integer -> String
iload n
    | n <= 3 = "iload_" ++ (show n)
    | otherwise = "iload " ++ (show n)

commutative :: OpType -> Bool
commutative Add = True
commutative Mul = True
commutative _ = False

jvmOp :: OpType -> String
jvmOp Add = "iadd"
jvmOp Sub = "isub"
jvmOp Mul = "imul"
jvmOp Div = "idiv"
